import java.io.*;
import java.util.*;

public class TrainingIris extends DataSet_iris {

    //-----------------------------------

    private int bestK = 0;

    //-----------------------------------
    
    public void train(int splitAmmount){
        //-----------------------------------
        int K = 5;
        double lastPrecision = 0;
        int totalDataSize = flower_list.size();
        //-----------------------------------
        if(splitAmmount == 0 || splitAmmount > totalDataSize){
            return;
        }
        //-----------------------------------
        int trainingDataSetSize = (totalDataSize - (totalDataSize % splitAmmount)) / splitAmmount;
        //-----------------------------------
        ArrayList<Flower> allData = new ArrayList<>(flower_list);
        ArrayList<Flower> trainingSet = new ArrayList<>();
        ArrayList<Flower> justTrained = new ArrayList<>();
        //-----------------------------------
        for(int j = 0; j < splitAmmount; j++) {
            //-----------------------------------
            for(int i = 0; i < trainingDataSetSize; i++) {
                int randomIndex = (int)(Math.random() * totalDataSize);
                Flower randomFlower = allData.get(randomIndex);
                if(justTrained.contains(randomFlower)) {
                    i++;
                    continue;
                } else {
                    trainingSet.add(randomFlower);
                    flower_list.remove(randomFlower);
                }
            }
            //-----------------------------------
            double error = 0;
            //-----------------------------------
            for(Flower flower : trainingSet){
                Map<String, Double> precisionCalculated = KNN(flower.getSepal_length(), flower.getSepal_width(), flower.getPetal_length(), flower.getPetal_width(), K);
                if(precisionCalculated.containsKey(flower.getIris_class())) {
                    error += 1 - precisionCalculated.get(flower.getIris_class());
                } else {
                    error++;
                }
            }
            //-----------------------------------
            
        }
        return;

    }

    //-----------------------------------

    public int getBestK(){
        return bestK;
    }

    //-----------------------------------
}


