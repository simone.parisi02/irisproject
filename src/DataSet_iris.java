import java.io.*;
import java.util.*;

public class DataSet_iris {
    //---------------------------------------

    ArrayList<Flower> flower_list = new ArrayList<>();

    //---------------------------------------

    public DataSet_iris(){}

    public DataSet_iris(DataSet_iris dataSet) {
        dataSet.flower_list.forEach((flower) -> addFlower(flower));
    }

    public DataSet_iris(ArrayList<Flower> flower_list) {
        flower_list.forEach((flower) -> addFlower(flower));
    }

    //---------------------------------------

    public void addFlower(Flower flower) {
        this.flower_list.add(flower);
    }

    public DataSet_iris loadCSV(String file_name) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file_name));
        String tmp_str;
        while ((tmp_str = reader.readLine()) != null) {
            addFlower(new Flower(tmp_str.split(",")));
        }
        reader.close();
        return this;
    }

    //---------------------------------------

    @Override public String toString() {
        String tmp_str = "";
        for(Flower flower : flower_list){
            tmp_str = tmp_str.concat(flower.toString() + System.lineSeparator());
        }
        return tmp_str;
    }

    //---------------------------------------

    public Map<String, Double> KNN(double sepal_length, double sepal_width, double petal_length, double petal_width){
        return KNN(sepal_length, sepal_width, petal_length, petal_width, 1);
    }

    public Map<String, Double> KNN(double sepal_length, double sepal_width, double petal_length, double petal_width, int k){
        
        if(k == 0){
            return null;
        }

        final Flower compareFlower = new Flower(sepal_length, sepal_width, petal_length, petal_width, null);
        ArrayList<FlowerDistance> distanceFlowers = new ArrayList<>();
        flower_list.forEach(f -> distanceFlowers.add(new FlowerDistance(f, f.distance(compareFlower))));
       
        distanceFlowers.sort((fd1, fd2) -> {
            double dist1 = fd1.getDistance();
            double dist2 = fd2.getDistance();
            return Double.compare(dist1, dist2);
        });
        
        if(k < distanceFlowers.size()){
            k = distanceFlowers.size();
        }

        return calculateProbability(distanceFlowers.subList(0, k));
    }

    //---------------------------------------

    public Map<String, Double> calculateProbability(List<FlowerDistance> list) {
		Map<String, Double> dict = new HashMap<String, Double>();
		list.forEach(flowerDist -> {
            String key = flowerDist.getFlower().getIris_class();
            if(dict.containsKey(key)) {
                dict.put(key, dict.get(key).doubleValue() + 1);
            } else {
                dict.put(key, 1d);
            }
        });
        dict.forEach((key, probability) -> dict.put(key, probability / list.size()));
		return dict;
    }
    
    //---------------------------------------
    
}


