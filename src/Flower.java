public class Flower {
    //---------------------------------------

    private double sepal_length, sepal_width, petal_length, petal_width;
    private String iris_class;

    //---------------------------------------

    public Flower(double sepal_length, double sepal_width, double petal_length, double petal_width, String iris_class) {
        this.sepal_length = sepal_length;
        this.sepal_width = sepal_width;
        this.petal_length = petal_length;
        this.petal_width = petal_width;
        this.iris_class = iris_class;
    }

    public Flower(Flower flower) {
        sepal_length = flower.sepal_length;
        sepal_width = flower.sepal_width;
        petal_length = flower.petal_length;
        petal_width = flower.petal_width;
        iris_class = flower.iris_class;
    }

    public Flower(String[] info) {
        sepal_length = Double.valueOf(info[0]);
        sepal_width = Double.valueOf(info[1]);
        petal_length = Double.valueOf(info[2]);
        petal_width = Double.valueOf(info[3]);
        iris_class = info[4];
    }

    //---------------------------------------

    public double getSepal_length() {
        return this.sepal_length;
    }

    public void setSepal_length(double sepal_length) {
        this.sepal_length = sepal_length;
    }

    public double getSepal_width() {
        return this.sepal_width;
    }

    public void setSepal_width(double sepal_width) {
        this.sepal_width = sepal_width;
    }

    public double getPetal_length() {
        return this.petal_length;
    }

    public void setPetal_length(double petal_length) {
        this.petal_length = petal_length;
    }

    public double getPetal_width() {
        return this.petal_width;
    }

    public void setPetal_width(double petal_width) {
        this.petal_width = petal_width;
    }

    public String getIris_class() {
        return this.iris_class;
    }

    public void setIris_class(String iris_class) {
        this.iris_class = iris_class;
    }

    //---------------------------------------

    @Override
    public String toString() {
        return "{" +
            "sepal_length='" + getSepal_length() + "'" +
            ",sepal_width='" + getSepal_width() + "'" +
            ",petal_length='" + getPetal_length() + "'" +
            ",petal_width='" + getPetal_width() + "'" +
            ",iris_class='" + getIris_class() + "'" +
            "}";
    }

    //---------------------------------------

    public double distance(Flower flower){
        final double sLength_v = Math.pow((sepal_length - flower.sepal_length), 2);
        final double sWidth_v = Math.pow((sepal_width - flower.sepal_width), 2);
        final double pLength_v = Math.pow((petal_length - flower.petal_length), 2);
        final double pWidth_v = Math.pow((petal_width - flower.petal_width), 2);
        final double distance_4D = Math.sqrt(sLength_v + sWidth_v + pLength_v + pWidth_v);
        return distance_4D;
    }

    //---------------------------------------

    public boolean equals(Object obj){
        return obj instanceof Flower ? ((Flower)obj).toString().equals(toString()) : false;
    }

    //---------------------------------------
}
