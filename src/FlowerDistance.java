class FlowerDistance implements Comparable<FlowerDistance>{
    //---------------------------------------

    private Flower flower;
    private double distance;

    //---------------------------------------

    public FlowerDistance(Flower flower, double distance){
        this.flower = new Flower(flower);
        this.distance = distance;
    }

    //---------------------------------------

    public double getDistance(){
        return distance;
    }

    public Flower getFlower(){
        return flower;
    }

    //---------------------------------------

    public String toString(){
        return flower.getIris_class() + " = " + distance + System.lineSeparator();
    }

    //---------------------------------------

    public int compareTo(FlowerDistance f1){
        return (int)(f1.distance - distance); 
    }
} 
